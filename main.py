
URL_GET_TOKEN = 'https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token'
URL_COSTS = 'https://cloud.redhat.com/api/cost-management/v1/reports/openshift/costs/'

BY_CLUSTER = {'group_by[cluster]':'*'}
BY_PROJECT = {'group_by[project]':'*'}


import requests, json, datetime, urllib
from pathlib import Path

day = (datetime.datetime.today() - datetime.timedelta(1)).strftime('%Y-%m-%d')

_token = None
def token():
  global _token
  if _token is None:
    data = {'grant_type':'refresh_token', 'client_id':'rhsm-api', 'refresh_token':RH_OFFLINE_TOKEN}
    x = requests.post(URL_GET_TOKEN, data=data)
    # throw the error up if the request failed
    x.raise_for_status()
    # otherwise return the 5 minute token
    _token = x.json()['access_token']
  return _token

def dl(method, name, params={}):
  headers = {'Authorization': f'Bearer {token()}'}
  url = method + '?' + urllib.parse.urlencode(params)
  print(f'Saving {url} to {name}')
  res = requests.get(url, headers=headers)
  # throw the error up if the request failed
  res.raise_for_status()
  write(res.json(), name)

def write(j, name):
  outdir = Path('out')
  outdir.mkdir(parents=True, exist_ok=True)
  with open( outdir / (name), 'w') as f:
    f.write(json.dumps(j, indent=2))

if '__main__' == __name__:
  BY_PROJECT.update({'start_date':day, 'end_date':day})
  dl(URL_COSTS, 'projects.json', params=BY_PROJECT)